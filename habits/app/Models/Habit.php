<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


class Habit extends Model
{
    use HasFactory;

    protected $fillable = [
        'habit',
        'interval',
        'streak',
        'is_habit',
        'created_at',
        'updated_at'
    ];


    public function entries()
    {

        return $this->hasMany(Entry::class, 'habit_id', 'id');
    }

    public function streak()
    {

        $now = Carbon::now();
        $entries = $this->entries()->where('created_at', '>=', $now->subDays($this->interval))->get();
        $count = 0;
        for ($i = 0; $i < $this->entries()->count(); $i++) {

            /* Each entity has a created_At timestamp. Check if theres exactly one entry per day. */
            if ($entries[$i]->created_at->format('Y-m-d') == $now->format('Y-m-d')) {
                $count++;
            }
        }
    }
}
