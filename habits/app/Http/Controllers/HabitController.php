<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreHabitRequest;
use App\Http\Requests\UpdateHabitRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Habit;
use App\Models\Entry;

class HabitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $habits = Habit::get();
        $entries = [];

        // Puts all the habits entries in a array
        foreach ($habits as $habit) {

            $habit->entries = Entry::where('habit_id', $habit->id)->get();
        }

        return $habits;
    }

    public function realhabits()
    {

        return Habit::where('is_habit', 1)->get();
    }

    public function habitEntries($id)
    {

        return Habit::find($id)->entries()->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorehabitRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreHabitRequest $req)
    {
        $habit = new Habit;

        $habit->habit       = $req->habit;
        $habit->interval    = $req->interval;
        $habit->created_at  = Carbon::now();

        try {
            $habit->save();
            return $habit;
        } catch (\Throwable $th) {
            return 'Det skjedde en feil' . $th->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\habit  $habit
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $habit = Habit::find($id);

        // Check if habit exists
        if ($habit === null) {
            return response()->json([
                'message' => 'Habit not found'
            ], 404);
        }

        return $habit;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\habit  $habit
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatehabitRequest  $request
     * @param  \App\Models\habit  $habit
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateHabitRequest $req, $id)
    {
        $habit = Habit::find($id);

        $habit->habit       = $req->habit      ?? $habit->habit;
        $habit->streak      = $req->streak     ?? $habit->streak;
        $habit->interval    = $req->interval   ?? $habit->interval;
        $habit->is_habit    = $req->is_habit   ?? $habit->is_habit;
        $habit->updated_at  = Carbon::now();

        try {
            $habit->update();
            return $habit;
        } catch (\Throwable $th) {
            return 'Det skjedde en feil ' . $th->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\habit  $habit
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $habit = Habit::find($id);

        if ($habit === null) {
            return response()->json([
                'message' => 'Habit not found'
            ], 404);
        }

        try {
            $habit->delete();
            return response()->json([
                'message' => "Habit: '$habit->habit' is deleted"
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message'   => 'Something is wrong',
                'error'     => $th->getMessage(),
            ], 404);
        }
    }

    public function habitClick($id)
    {

        $habit = Habit::find($id);

        // Creates a new Habit Entry record
        $entry = new Entry;
        $entry->habit_id = $habit->id;
        $entry->created_at = Carbon::now();
        $entry->save();

        // Updates the HABIT
        $entries = Entry::where('habit_id', $habit->id)->get();
        $habit->streak = $entries->count();
        $habit->updated_at = Carbon::now();

        // Checks if the habit is close to get a streak
        if ($habit->streak >= 30) {
            $habit->is_habit = 1;
        } elseif ($habit->streak <= 29) {
            $habit->is_habit = 0;
        } else {
            $habit->is_habit = 0;
        }

        try {
            $habit->update();
            return $habit;
        } catch (\Throwable $th) {
            return 'Det skjedde en feil' . $th->getMessage();
        }
    }

    public function checkIfHabits()
    {

        $habits = Habit::get();

        foreach ($habits as $habit) {
            if ($habit->streak()->count() > 29) {
                $habit->is_habit = 1;
            }
            $habit->update();
        }
        return $habit;
    }
}
