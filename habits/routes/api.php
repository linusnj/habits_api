<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// HABITS
Route::get('/habits', [App\Http\Controllers\HabitController::class, 'index']);
Route::get('/realhabits', [App\Http\Controllers\HabitController::class, 'realhabits']);
Route::prefix('/habit')->group(function () {
    Route::get('/{id}', [App\Http\Controllers\HabitController::class, 'show']);
    Route::post('/store', [App\Http\Controllers\HabitController::class, 'store']);
    Route::post('/click/{id}', [App\Http\Controllers\HabitController::class, 'habitClick']);
    Route::put('/{id}', [App\Http\Controllers\HabitController::class, 'update']);
    Route::delete('/{id}', [App\Http\Controllers\HabitController::class, 'destroy']);
});

Route::get('/check', [App\Http\Controllers\HabitController::class, 'checkIfHabits']);
