<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class IntervalType extends Enum
{
    const EveryDay          = 0;
    const EveryOtherDay     = 1;
    const EveryWeek         = 2;
    const EveryOtherWeek    = 3;
    const EveryMonth        = 4;
    const EveryOtherMonth   = 5;
}
